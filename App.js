//
//  App.js
//  Mitras
//
//  Created by Author.
//  Copyright © 2018 [Company]. All rights reserved.
//

import { createStackNavigator, createAppContainer } from "react-navigation"
import LoadingTwo from "./App/LoadingTwo/LoadingTwo"
import React from "react"
import Onboarding from "./App/Onboarding/Onboarding"

const PushRouteOne = createStackNavigator({
	LoadingTwo: {
		screen: LoadingTwo,
	},
	Onboarding: {
		screen: Onboarding,
	},
}, {
	initialRouteName: "LoadingTwo",
})

const RootNavigator = createStackNavigator({
	PushRouteOne: {
		screen: PushRouteOne,
	},
}, {
	mode: "modal",
	headerMode: "none",
	initialRouteName: "PushRouteOne",
})

const AppContainer = createAppContainer(RootNavigator)



export default class App extends React.Component {

	render() {
	
		return <AppContainer/>
	}
}
