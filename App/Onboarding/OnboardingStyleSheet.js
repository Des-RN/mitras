//
//  OnboardingStyleSheet.js
//  Mitras
//
//  Created by Author.
//  Copyright © 2018 [Company]. All rights reserved.
//

import { StyleSheet } from "react-native"

const styles = StyleSheet.create({
	onboardingView: {
		width: "100%",
		height: "100%",
	},
	onboardingViewLinearGradient: {
		flex: 1,
	},
	path3Image: {
		resizeMode: "cover",
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		right: 0,
		top: 0,
		height: 499,
	},
	slide1View: {
		backgroundColor: "transparent",
		width: 282,
		height: 327,
	},
	featureImage1Image: {
		backgroundColor: "transparent",
		resizeMode: "cover",
		width: null,
		height: 231,
	},
	featureDescText: {
		backgroundColor: "transparent",
		color: "black",
		fontFamily: "Lato-Regular",
		fontSize: 18,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "center",
		marginLeft: 7,
		marginRight: 7,
	},
	paginationImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: 41,
		height: 10,
		marginTop: 26,
	},
	signupView: {
		backgroundColor: "white",
		borderRadius: 5,
		shadowColor: "rgba(0, 0, 0, 0.2)",
		shadowRadius: 50,
		shadowOpacity: 1,
		width: 148,
		height: 60,
		flexDirection: "row",
		alignItems: "center",
	},
	iconSignUpImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: 19,
		height: 19,
		marginLeft: 29,
	},
	signUpText: {
		color: "rgb(134, 68, 211)",
		fontFamily: "Lato-Heavy",
		fontSize: 15,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
		lineHeight: 20,
		backgroundColor: "transparent",
		marginRight: 28,
	},
	loginView: {
		backgroundColor: "white",
		borderRadius: 5,
		shadowColor: "rgba(0, 0, 0, 0.2)",
		shadowRadius: 50,
		shadowOpacity: 1,
		width: 148,
		height: 60,
		flexDirection: "row",
		alignItems: "center",
	},
	iconLogInImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: 15,
		height: 17,
		marginLeft: 37,
	},
	logInText: {
		backgroundColor: "transparent",
		color: "rgb(134, 68, 211)",
		fontFamily: "Lato-Heavy",
		fontSize: 15,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
		lineHeight: 20,
		marginRight: 33,
	},
	copyrightText: {
		backgroundColor: "transparent",
		color: "white",
		fontFamily: "Lato-Regular",
		fontSize: 15,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "center",
		alignSelf: "center",
		marginBottom: 20,
	},
})

export default styles
