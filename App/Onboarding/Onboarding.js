//
//  Onboarding
//  Mitras
//
//  Created by Author.
//  Copyright © 2018 [Company]. All rights reserved.
//

import { Image, Text, View } from "react-native"
import styles from "./OnboardingStyleSheet"
import React from "react"
import LinearGradient from "react-native-linear-gradient"


export default class Onboarding extends React.Component {

	static navigationOptions = ({ navigation }) => {
	
		const { params = {} } = navigation.state
		return {
				header: null,
				headerLeft: null,
				headerRight: null,
			}
	}

	constructor(props) {
		super(props)
	}

	componentDidMount() {
	
	}

	render() {
	
		return <LinearGradient
				start={{
					x: 0.31,
					y: 1.1,
				}}
				end={{
					x: 0.69,
					y: -0.1,
				}}
				locations={[0, 1]}
				colors={["rgb(213, 98, 247)", "rgb(131, 73, 207)"]}
				style={styles.onboardingViewLinearGradient}>
				<View
					style={styles.onboardingView}>
					<View
						pointerEvents="box-none"
						style={{
							height: 499,
						}}>
						<Image
							source={require("./../../assets/images/path-3.png")}
							style={styles.path3Image}/>
						<View
							pointerEvents="box-none"
							style={{
								position: "absolute",
								alignSelf: "center",
								width: 282,
								top: 109,
								height: 363,
								alignItems: "center",
							}}>
							<View
								style={styles.slide1View}>
								<Image
									source={require("./../../assets/images/feature-image1.png")}
									style={styles.featureImage1Image}/>
								<View
									style={{
										flex: 1,
									}}/>
								<Text
									style={styles.featureDescText}>Lorem ipsum dolar sit lorem ipsum lorem ipsum</Text>
							</View>
							<Image
								source={require("./../../assets/images/pagination.png")}
								style={styles.paginationImage}/>
						</View>
					</View>
					<View
						style={{
							flex: 1,
						}}/>
					<View
						pointerEvents="box-none"
						style={{
							height: 60,
							marginLeft: 26,
							marginRight: 26,
							marginBottom: 58,
							flexDirection: "row",
							alignItems: "flex-end",
						}}>
						<View
							style={styles.signupView}>
							<Image
								source={require("./../../assets/images/icon-sign-up.png")}
								style={styles.iconSignUpImage}/>
							<View
								style={{
									flex: 1,
								}}/>
							<Text
								style={styles.signUpText}>SIGN UP</Text>
						</View>
						<View
							style={{
								flex: 1,
							}}/>
						<View
							style={styles.loginView}>
							<Image
								source={require("./../../assets/images/icon-log-in.png")}
								style={styles.iconLogInImage}/>
							<View
								style={{
									flex: 1,
								}}/>
							<Text
								style={styles.logInText}>LOG IN</Text>
						</View>
					</View>
					<Text
						style={styles.copyrightText}>© 2019 Design Studio</Text>
				</View>
			</LinearGradient>
	}
}
