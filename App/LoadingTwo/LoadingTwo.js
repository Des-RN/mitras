//
//  LoadingTwo
//  Mitras
//
//  Created by Author.
//  Copyright © 2018 [Company]. All rights reserved.
//

import LinearGradient from "react-native-linear-gradient"
import React from "react"
import styles from "./LoadingTwoStyleSheet"
import { TouchableOpacity, Image, View, Animated, Easing, Text } from "react-native"


export default class LoadingTwo extends React.Component {

	static navigationOptions = ({ navigation }) => {
	
		const { params = {} } = navigation.state
		return {
				header: null,
				headerLeft: null,
				headerRight: null,
			}
	}

	constructor(props) {
		super(props)
		this.state = {
			nameButtonTranslateX: new Animated.Value(-1),
			nameButtonOpacity: new Animated.Value(-1),
			logoImageTranslateX: new Animated.Value(-1),
			logoImageOpacity: new Animated.Value(-1),
		}
	}

	componentDidMount() {
	
	}

	onNamePressed = () => {
	
		const { navigate } = this.props.navigation
		
		navigate("Onboarding")
	}

	startAnimationOne() {
	
		// Set animation initial values to all animated properties
		this.state.logoImageTranslateX.setValue(0)
		this.state.logoImageOpacity.setValue(0)
		this.state.nameButtonTranslateX.setValue(0)
		this.state.nameButtonOpacity.setValue(0)
		
		// Configure animation and trigger
		Animated.parallel([Animated.parallel([Animated.timing(this.state.logoImageTranslateX, {
			duration: 1000,
			easing: Easing.bezier(0.22, 0.61, 0.61, 1),
			toValue: 1,
		}), Animated.timing(this.state.logoImageOpacity, {
			duration: 1000,
			easing: Easing.bezier(0.22, 0.61, 0.61, 1),
			toValue: 1,
		})]), Animated.parallel([Animated.timing(this.state.nameButtonTranslateX, {
			duration: 1000,
			easing: Easing.bezier(0.22, 0.61, 0.61, 1),
			toValue: 1,
		}), Animated.timing(this.state.nameButtonOpacity, {
			duration: 1000,
			easing: Easing.bezier(0.22, 0.61, 0.61, 1),
			toValue: 1,
		})])]).start()
	}

	render() {
	
		return <LinearGradient
				start={{
					x: 0.31,
					y: 1.1,
				}}
				end={{
					x: 0.69,
					y: -0.1,
				}}
				locations={[0, 1]}
				colors={["rgb(213, 98, 247)", "rgb(131, 73, 207)"]}
				style={styles.loadingViewLinearGradient}>
				<View
					style={styles.loadingView}>
					<Animated.View
						style={[{
							opacity: this.state.logoImageOpacity.interpolate({
								inputRange: [-1, 0, 0.2, 1],
								outputRange: [1, 1, 1, 0],
							}),
							transform: [{
								translateX: this.state.logoImageTranslateX.interpolate({
									inputRange: [-1, 0, 0.2, 1],
									outputRange: [0.01, 0, 20, -2000],
								}),
							}],
						}, styles.logoImageAnimated]}>
						<Image
							source={require("./../../assets/images/logo.png")}
							style={styles.logoImage}/>
					</Animated.View>
					<Animated.View
						style={[{
							opacity: this.state.nameButtonOpacity.interpolate({
								inputRange: [-1, 0, 0.2, 1],
								outputRange: [1, 1, 1, 0],
							}),
							transform: [{
								translateX: this.state.nameButtonTranslateX.interpolate({
									inputRange: [-1, 0, 0.2, 1],
									outputRange: [0.01, 0, -20, 2000],
								}),
							}],
						}, styles.nameButtonAnimated]}>
						<TouchableOpacity
							onPress={this.onNamePressed}
							style={styles.nameButton}>
							<Text
								style={styles.nameButtonText}>Mitr</Text>
						</TouchableOpacity>
					</Animated.View>
					<View
						style={{
							flex: 1,
						}}/>
					<Text
						style={styles.copyrightText}>© 2019 Design Studio</Text>
				</View>
			</LinearGradient>
	}
}
