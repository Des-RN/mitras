//
//  LoadingTwoStyleSheet.js
//  Mitras
//
//  Created by Author.
//  Copyright © 2018 [Company]. All rights reserved.
//

import { StyleSheet } from "react-native"

const styles = StyleSheet.create({
	loadingViewLinearGradient: {
		flex: 1,
	},
	loadingView: {
		width: "100%",
		height: "100%",
		alignItems: "center",
	},
	logoImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: "100%",
		height: "100%",
	},
	logoImageAnimated: {
		width: 149,
		height: 149,
		marginTop: 129,
	},
	nameButton: {
		backgroundColor: "transparent",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		width: "100%",
		height: "100%",
	},
	nameButtonText: {
		color: "white",
		fontFamily: "Lato-Heavy",
		fontSize: 42,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
	},
	nameButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	nameButtonAnimated: {
		width: 80,
		height: 50,
		marginTop: 68,
	},
	copyrightText: {
		color: "white",
		fontFamily: "Lato-Regular",
		fontSize: 15,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "center",
		backgroundColor: "transparent",
		marginBottom: 20,
	},
})

export default styles
